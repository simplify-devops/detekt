# detekt helpers 2020-10-23 by @k33g | on gitlab.com 
FROM alpine:latest

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN apk --update add --no-cache bash curl util-linux \
    nodejs npm \
    openjdk11
RUN curl -sSLO https://github.com/detekt/detekt/releases/download/v1.14.2/detekt
RUN chmod a+x detekt
RUN mv detekt /usr/local/bin/

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]
