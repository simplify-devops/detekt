#!/usr/bin/env node

const fs = require("fs")
const { exec } = require("child_process")

const severityValues = ["info", "minor", "major", "critical", "blocker"]

let cmdArgs = process.argv.slice(2);
let directory = cmdArgs[0] || "src"
let reportName = cmdArgs[1] || "./detekt.txt" 

// detekt --input kotlin/funky-regulator/src --report txt:detekt.txt
let cmd = `detekt --input ${directory} --report txt:${reportName}`

exec(cmd, (error, stdout, stderr) => {

  if (error) {
    console.log(`😡 error(s) found: ${error.message}`)

    try {
      let content = fs.readFileSync(reportName)
      
      /* --- helpers --- */
      let btoa = (string) => Buffer.from(string).toString("base64")
      let fingerprint = (description, path, line, column) => `${btoa(description)}-${btoa(path)}-${btoa(line)}-${btoa(column)}`
    
      let detektResults = content.toString().split("\n").filter(row => row !== "").map(row => {

        //console.log("🤖", row)
        
        let first_step = row.split(" - [")
        let rule = first_step[0]
        let second_step = "[" + first_step[1]
        let third_step = second_step.split(" - ")
        let details = third_step[0].split(" at ")

        let member = details[0]
        let path = details[1].split(":")
        let file = path[0]
        let line = path[1]
        let column = path[2]

        let signature = third_step[1].split("Signature=")[1]

        return {
          rule: rule,
          details: {
            member: member,
            file: file,
            line: line,
            column: column
          },
          signature: signature
        }
      })

      console.log("📝", detektResults)

      if(detektResults.length==0) {
        /* --- generate empty gl-code-quality-report.json --- */
        fs.writeFileSync("./gl-code-quality-report.json", "[]")
      } else {
        /* --- generate gl-code-quality-report.json --- */
        let codeQualityData = detektResults.map(item => {
          let fp = fingerprint(item.rule, item.details.file, item.details.line.toString(), item.details.column.toString())
          return {
            description: `📝 ${item.rule} (${item.details.line}:${item.details.column}): ${item.signature}`,
            fingerprint: fp,
            location: {
              path: item.details.file.replace(process.env.CI_PROJECT_DIR ,""),
              lines: {
                begin: item.details.line
              }
            },
            severity: severityValues[0] // detekt does not provide this data, so severity="info"
          }
        })
      
        fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(codeQualityData, null, 2))
      
        return
      }      

    } catch(error) {
      /* --- generate empty gl-code-quality-report.json --- */
      console.log("😡", error)
      console.log("🖐️ generate empty report")
      fs.writeFileSync("./gl-code-quality-report.json", "[]")
    }
    

  } else { 
    /* --- generate empty gl-code-quality-report.json --- */
    fs.writeFileSync("./gl-code-quality-report.json", "[]")
  }

  if (stderr) {
    console.log(`stderr: ${stderr}`)
    return
  }
  console.log(`stdout: ${stdout}`)
})


